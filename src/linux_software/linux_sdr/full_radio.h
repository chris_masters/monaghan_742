#include "chris_ui.h"

#define _BSD_SOURCE

// Constants
#define RADIO_TUNER_FAKE_ADC_PINC_OFFSET 0
#define RADIO_TUNER_TUNER_PINC_OFFSET 1
#define RADIO_TUNER_CONTROL_REG_OFFSET 2
#define RADIO_TUNER_TIMER_REG_OFFSET 3
#define RADIO_PERIPH_ADDRESS 0x43c00000

#define FIFO_PERIPH_ADDR 0x43C10000
#define FIFO_DATA_REG_OFFSET 0
#define FIFO_FILL_LVL_OFFSET 1

#define SAMPS_PER_PKT 256
#define SAMPLE_RATE 125000000
#define DDS_PINC 0x07FFFFFF


// Global variables: used as synchronization between threads
extern volatile int streamer_run;
extern char ip_addr[MAX_IP_SZ];

// Function prototypes
void load_buff(unsigned short frm_counter,
               char *buff,
               int num_samps,
               volatile unsigned int *fifop);
volatile unsigned int * get_a_pointer(unsigned int phys_addr);
void *udp_streamer(void *vargp);

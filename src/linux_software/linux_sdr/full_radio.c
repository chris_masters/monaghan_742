#include <stdio.h>
#include <sys/mman.h> 
#include <fcntl.h> 
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "full_radio.h"
#include "chris_udp.h"


// Function to initialize and load a buffer with data from the radio
void load_buff(unsigned short frm_counter,
               char *buff,
               int num_samps,
               volatile unsigned int *fifop)
{
  // Local variables
  int i;
  unsigned int curr_samp = 0;
  unsigned short I, Q;

  // Clear the buffer
  bzero(buff, num_samps);
  
  // Load the frame counter
  buff[0] = frm_counter;
  buff[1] = frm_counter >> 8;
  
  // Load data from the radio peripheral
  for (i=0; i<num_samps; i++) {
    // Read the next word out of the fifo
    curr_samp = fifop[FIFO_DATA_REG_OFFSET];
    I = curr_samp;
    Q = curr_samp >> 16;
    
    // Stuff each sample into the buffer I,Q,I,Q,... little endian
    buff[4*i+2] = I;
    buff[4*i+3] = I >> 8;
    buff[4*i+4] = Q;
    buff[4*i+5] = Q >> 8;
  }
}


// Use /dev/mem to get a pointer to a physical address
volatile unsigned int * get_a_pointer(unsigned int phys_addr)
{

	int mem_fd = open("/dev/mem", O_RDWR | O_SYNC); 
	void *map_base = mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, mem_fd, phys_addr); 
	volatile unsigned int *radio_base = (volatile unsigned int *)map_base; 
	return (radio_base);
}


// UDP streamer: this function is called as a separate thread from main.
//  It handles reading data from the radio peripheral and streaming it
//  out via UDP packets to the specified client
void *udp_streamer(void *vargp) {
  // Variables
  int radio_fill_lvl = 0;
  unsigned short frame_counter = 0;
  char data_buff[SAMPS_PER_PKT*4+2];
  volatile unsigned int *radio_base_addr = get_a_pointer(RADIO_PERIPH_ADDRESS);
  volatile unsigned int *fifo_base_addr = get_a_pointer(FIFO_PERIPH_ADDR);

  int socket_desc;
  struct sockaddr_in client_addr;
  int client_struct_length = sizeof(client_addr);
  

  printf("UDP thread started.\r\n");
  
  
  while(1) {
    if (streamer_run != 0) {
      // Setup the UDP socket and client address
      if (create_socket(&socket_desc, &client_addr, ip_addr, PORT_NUM) < 0) {
        printf("Error creating socket\r\n");
        return NULL;
      }

      while (streamer_run != 0) {
        // Wait for 1 packet's worth of data
        while (radio_fill_lvl < SAMPS_PER_PKT)
          radio_fill_lvl = *(fifo_base_addr+FIFO_FILL_LVL_OFFSET);
        radio_fill_lvl = 0; //Reset fill level for the next pass

        // Clear/load the data into the data buffer
        load_buff(frame_counter, data_buff, SAMPS_PER_PKT, fifo_base_addr);

        // Attempt to send the UDP packet
        if (sendto(socket_desc, data_buff, sizeof(data_buff), 0,
                   (struct sockaddr*)&client_addr, client_struct_length) < 0) {
          printf("Error sending UDP packet %d\r\n",frame_counter);
        }

        // Increment the frame counter
        frame_counter++;
      }
      
      close(socket_desc);
    }
    else {
      // Sleep for duration of ~1 packet
      usleep(5250);
    }
  }
}


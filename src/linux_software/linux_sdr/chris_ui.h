#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>

#define MAX_IP_SZ 17

// Globals
extern char ip_addr[MAX_IP_SZ];

// Function prototypes
void print_menu();
char get_char_nb();
void main_lobby(volatile unsigned int *radio_base_addr);
int set_manual_freq();
int hz_to_pinc(int freq_hz);
void reset_phase(volatile unsigned int *radio_base_addr);
void update_tune_dds_freq(volatile unsigned int *radio_base_addr,
                          int freq_hz);
void toggle_streaming();
void update_ip_addr();

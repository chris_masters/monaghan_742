#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "chris_udp.h"

// Function to parse the command line for dest. ip address and num packets
//int parse_cmd_line(int argc, char *argv[], char **ip_addr) {
//  // Error checking
//  if (!(argc==2)) {
//    printf("Error: incorrect number of arguments! \r\n");
//    printf("Please enter a destination IP address\r\n");
//    return -1;
//  }
//
//  if (strlen(argv[1]) > 15) {
//    printf("Error: Specified IP address is too long\r\n");
//    return -1;
//  }
//
//  // Assign the IP addr
//  *ip_addr = argv[1];
//
//  return 0;    
//}


// Function to load sample data from a text file
//int load_test_data(FILE *fp, char *buffer, int len) {
//  int i;
//  char c;
//  
//  for (i=0; i<len; i++) {
//    // Get the next char from the file
//    c = fgetc(fp);
//    
//    // If we reach the end of the file, rewind back to the beginning
//    if (feof(fp)) {
//      fseek(fp, 0, SEEK_SET);
//      c = fgetc(fp);
//    }
//
//    // Load the data into the buffer
//    buffer[i] = c;
//  }
//
//  return 0;
//}


// Function to create/setup the socket and client address
int create_socket(int *socket_desc, struct sockaddr_in *client_addr,
                  char *ip_addr, int port_num) {
  // Create UDP socket
  printf("Creating socket...");
  *socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (*socket_desc < 0) {
    printf("Failed! Error creating socket\r\n");
    return -1;
  }
  printf("Success!\r\n");

  // Set port and IP address
  client_addr->sin_family = AF_INET;
  client_addr->sin_addr.s_addr = inet_addr(ip_addr);
  client_addr->sin_port = htons(port_num);

  return 0;
}


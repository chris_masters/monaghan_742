#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

// Constant declarations
#define PORT_NUM 25344
#define PAYLOAD_SIZE 1024

// Function prototypes
//int parse_cmd_line(int argc, char *argv[], char **ip_addr);
//int load_test_data(FILE *fp, char *buffer, int len);
int create_socket(int *socket_desc, struct sockaddr_in *client_addr,
                  char *ip_addr, int port_num);
//int send_udp_pkt(char *payload, int len);


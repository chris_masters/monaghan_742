#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <termios.h>
#include "chris_udp.h"
#include "full_radio.h"
#include "chris_ui.h"



// Globals (declared in full_radio.c as external so that they are visible there and here)
volatile int streamer_run = 0;
char ip_addr[MAX_IP_SZ] = "192.168.26.5";


// Main thread: kicks off the upd thread and runs the user interface
int main (int argc, char *argv[])
{
  // Variables
  pthread_t thread_id;
  volatile unsigned int *radio_base_addr = get_a_pointer(RADIO_PERIPH_ADDRESS);

  printf("IP address = %s\r\n", ip_addr);

  // Disable streaming and start the UDP streamer thread
  streamer_run = 0;
  printf("Starting UDP thread...\r\n");
  pthread_create(&thread_id, NULL, udp_streamer, NULL);
  sleep(1);

  // User interface
  print_menu();
  while (1) {
    // Wait for user input and decode choice
    main_lobby(radio_base_addr);
  }
}

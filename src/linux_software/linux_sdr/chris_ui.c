#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <math.h>
#include "full_radio.h"
#include "chris_ui.h"


// "Local" globals
int fadc_freq_hz = 1001000;


// Prints the main menu options
void print_menu() {
  printf("\n\r\n\r");
  printf("Press 't' to tune the radio to a new frequency\n\r");
	printf("Press 'f' to set the fake ADC to a new frequency.\n\r");
	printf("Press 'U/u' to increase fake ADC frequency by 1000/100 Hz.\n\r");
	printf("Press 'D/d' to decrease fake ADC frequency by 1000/100 Hz.\n\r");
	printf("Press 'r' to reset the phase.\n\r");
  printf("Press 'i' to specify a new target IP address.\n\r");
  printf("Press 'e' to toggle between sending or not sending UDP packets.\n\r");
	printf("Press [space] to repeat this menu.\n\r");
	printf("\n\r");
}


/* Version of getc() that does not wait for carrige return
   Code inspired by this post: https://stackoverflow.com/
   questions/1798511/how-to-avoid-pressing-enter-with-getchar
   -for-reading-a-single-character-only */
char get_char_nb() {
  char c;
  static struct termios oldt, newt;

  // Get params of the current terminal and copy
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;

  // ICANON normally waits for "\n" before returning
  newt.c_lflag &= ~(ICANON);

  // Send new settings to STDIN immediatley
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);

  // Now call getchar which will return with the 1st keypress
  c = getc(stdin);

  // Restore the old settings
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

  return c;
}


/* Main lobby function: waits for user input, decodes and calls
   appropriate handler function. Unsupported choices just returns
   to main menu */
void main_lobby(volatile unsigned int *radio_base_addr) {
  char c;
  int update_fadc_pinc = 0;
  int update_tune_pinc = 0;
  int fadc_pinc = 0;
  //int tune_pinc = 0;
  int tune_freq_hz = 0;

  c = get_char_nb();
  printf("\n\r");

  // Decode user input
	switch (c) {
	case 'f':
		fadc_freq_hz = set_manual_freq();
		if (fadc_freq_hz == -1)
      printf("Invalid frequency; doing nothing.\n\r");
		else update_fadc_pinc = 1;
		break;
	case 'U':
		fadc_freq_hz += 1000;
		update_fadc_pinc = 1;
		break;
	case 'u':
		fadc_freq_hz += 100;
		update_fadc_pinc = 1;
		break;
	case 'D':
		fadc_freq_hz -= 1000;
		update_fadc_pinc = 1;
		break;
	case 'd':
		fadc_freq_hz -= 100;
		update_fadc_pinc = 1;
		break;
	case 'r':
		reset_phase(radio_base_addr);
		break;
	case 't':
		tune_freq_hz = set_manual_freq();
		update_tune_dds_freq(radio_base_addr,
                         -tune_freq_hz);	// Negate the input frequency (mix DOWN)
		break;
  case 'i':
    update_ip_addr();
    break;
  case 'e':
    toggle_streaming();
    break;
	case ' ':
		print_menu();
		break;
	}
  
	if (update_fadc_pinc) {
		// Fake ADC frequency has changed. Convert to PINC
		fadc_pinc = hz_to_pinc(fadc_freq_hz);

		// Inform the user of the change
		printf("\n\r");
		printf("Updating fake ADC frequency:\n\r");
		printf("Fake ADC freq\t\t= %d\n\r", fadc_freq_hz);
		printf("Fake ADC phase_inc\t= %d\n\r", fadc_pinc);

		// Write out new PINC to the DDS
    *(radio_base_addr+RADIO_TUNER_FAKE_ADC_PINC_OFFSET) =
      fadc_pinc;
	}
}


// Prompts the user to enter a new frequency in Hz and returns this value
//	as an integer. Return value will be -1 if there is an error reading
//	e.g., user enters an illegal char or max string size is reached.
int set_manual_freq() {
	int receiving = 1;
	char c;
	int new_freq = 0;
	char rx_str[10] = "";
	int char_count = 0;

	printf("Enter a frequency in Hz: \n\r");

	//Receive chars from the UART until CR or max size reached
	while (receiving) {
		// Read the next char
		c = get_char_nb();

		// Check that the char is a number or CR
		if( !(c >= '0' && c <= '9' ) && !(c == '\n') && !(c == '\r')) {
			printf("\n\rInvalid character received! Frequency not loaded.\n\r");
			// Return error state; leave frequency unchanged
			return -1;
		}

		// Check if user entered CR
		if ((c == '\n') || (c == '\r')) {
			printf("\n\rReturn received, done reading frequency.\n\r");
			receiving = 0;
		} else {
			// Append to string
			if (char_count <= 9) {
				strncat(rx_str,&c,1);
			}
			char_count++;
		}
	}

	// Pull the resulting integer out of the string
	sscanf(rx_str,"%d",&new_freq);

	// Check if integer is out of bounds or too many chars entered
	if ((new_freq > SAMPLE_RATE) || (char_count > 9)) {
		printf("Entered frequency is too large. Must be <= %dMHz\n\r",SAMPLE_RATE);
		printf("Setting frequency to max (%dMHz)\n\r",SAMPLE_RATE);
		return SAMPLE_RATE;
	} else {
		// Else return the frequency entered
		return new_freq;
	}
}


// Converts frequency in Hz to a DDS phase increment value. Values
//	larger than the max DDS phase increment value are "aliased" into
//	range using mod.
int hz_to_pinc(int freq_hz) {
	double pinc_dbl;
	long long pinc_long;
	double Fs = (double)SAMPLE_RATE;

	// First check that the frequency specified is legal, correct it if not
	if (abs(freq_hz) > DDS_PINC) {
		freq_hz = freq_hz % DDS_PINC;
	}

	// Convert to phase increment and cast to an int. Same math described
	// 	in the matlab code
	pinc_dbl = (((double)freq_hz)/Fs)*pow(2.0,27.0); // ((double)(1<<27))
	pinc_dbl = pinc_dbl+0.5; //round to nearest integer Hz
	pinc_long = (long long)pinc_dbl;

	return (int)pinc_long;
}


// Asserts the DDS reset line for 1000us and then releases it. Note that
//	the same reset signal is tied to the low-level DAC interface in the
//	FPGA fabric i.e., both get reset by this function.
void reset_phase(volatile unsigned int *radio_base_addr) {
	printf("Asserting DDS async reset...");

  // Assert the DDS reset via the control reg
  *(radio_base_addr+RADIO_TUNER_CONTROL_REG_OFFSET) = 1;

  // Sleep for a few hundred microseconds
  usleep(200);

  // Release the reset
  *(radio_base_addr+RADIO_TUNER_CONTROL_REG_OFFSET) = 0;
  
	printf("Done.\n\r");
}


// Writes out a new tuner DDS Phase increment value.
void update_tune_dds_freq(volatile unsigned int *radio_base_addr,
                          int freq_hz) {
	int tune_pinc = 0;

	// Convert Hz to PINC
	tune_pinc = hz_to_pinc(freq_hz);

	// Inform the user of the change
	printf("\n\r");
	printf("updating tuner DDS frequency:\n\r");
	printf("Tuner DDS freq\t\t= %d\n\r",freq_hz);
	printf("Tuner DDS phase_inc\t= %d\n\r",tune_pinc);

	// Write out new PINC to the tuner DDS
  *(radio_base_addr+RADIO_TUNER_TUNER_PINC_OFFSET) = tune_pinc;
	//XGpio_WriteReg(DDS_REG_BASEADDR,
	//		XGPIO_DATA2_OFFSET,
	//		tune_pinc);
}


// Turns UDP streaming on/off
void toggle_streaming() {
  streamer_run = ~streamer_run;

  if (streamer_run)
    printf("UDP streaming enabled.\n\r");
  else
    printf("UDP streaming disabled.\n\r");
}


/* Updates the target IP address to which UDP packets will
   be sent. Querys the user to input the new address, does
   some validity checking and then assigns the new address */
void update_ip_addr() {
  // Temp variable for user-input address
  char ip_tmp[MAX_IP_SZ];
  
  // Tell user to input new ip_addr
  printf("Input the new target IP address: ");

  // Scan in the new addr
  if (fgets(ip_tmp, MAX_IP_SZ, stdin) == NULL) {
    printf("No input!\r\n");
    return;
  }

  if (ip_tmp[strlen(ip_tmp)-1] != '\n') {
    printf("Specified IP address is too long! Max length is %d characters.\n\r",
           MAX_IP_SZ-1);
    return;
  }

  strncpy(ip_addr, ip_tmp, 15);
  printf("New IP address = %s\n\r", ip_addr);
  return;
}

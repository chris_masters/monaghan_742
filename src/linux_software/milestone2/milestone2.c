#include <stdio.h>
#include <sys/mman.h> 
#include <fcntl.h> 
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#define _BSD_SOURCE

#define RADIO_TUNER_FAKE_ADC_PINC_OFFSET 0
#define RADIO_TUNER_TUNER_PINC_OFFSET 1
#define RADIO_TUNER_CONTROL_REG_OFFSET 2
#define RADIO_TUNER_TIMER_REG_OFFSET 3
#define RADIO_PERIPH_ADDRESS 0x43c00000

#define FIFO_PERIPH_ADDR 0x43C10000
#define FIFO_DATA_REG_OFFSET 0
#define FIFO_FILL_LVL_OFFSET 1

// Global variable definitions (for thread sync)
int read_samps = 0;

// the below code uses a device called /dev/mem to get a pointer to a physical
// address.  We will use this pointer to read/write the custom peripheral
volatile unsigned int * get_a_pointer(unsigned int phys_addr)
{

	int mem_fd = open("/dev/mem", O_RDWR | O_SYNC); 
	void *map_base = mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, mem_fd, phys_addr); 
	volatile unsigned int *radio_base = (volatile unsigned int *)map_base; 
	return (radio_base);
}

// Trying out multi-threading: start a 2nd thread to print the total number
//  of samples read once per second
void *samp_reporter(void *vargp)
{
  int i;
  while (1) {
    printf("Total number of samples read = %d\r\n", read_samps);
    sleep(1);
  }
}


int main()
{
  // Constants
  int samps_to_read = 480000;
  int samps_per_pkt = 256;
  double clk_per = 1/125e6;
  
  //Variables
  int i,j;
  int fill_lvl = 0;
  unsigned int rd_wrd = 0;
  int secs_prev = 0;
  unsigned int start_clks, end_clks;
  double elapsed_time = 0;
  volatile unsigned int *radio_base_addr = get_a_pointer(RADIO_PERIPH_ADDRESS);
  volatile unsigned int *fifo_base_addr = get_a_pointer(FIFO_PERIPH_ADDR);
  pthread_t thread_id;

  //Print message to indicate test is starting
  printf("\r\n\r\n\r\nHello, I am going to read 10 seconds worth of data now...\r\n");

  //Start the reporter thread
  pthread_create(&thread_id, NULL, samp_reporter, NULL);
  
  //Start the stopwatch
  start_clks = radio_base_addr[RADIO_TUNER_TIMER_REG_OFFSET];

  //Read loop
  while (read_samps < samps_to_read) {
    //Poll the fill reg until we have 256 samples (using pointer indirection style here)
    while (fill_lvl < samps_per_pkt) fill_lvl = *(fifo_base_addr+FIFO_FILL_LVL_OFFSET);
    fill_lvl = 0; //Reset fill_level for the next pass

    //Read a packet's worth of data out of the FIFO (using array indexing style here)
    for (i=0; i<samps_per_pkt; i++) rd_wrd = fifo_base_addr[FIFO_DATA_REG_OFFSET];

    //Increment the total samples read
    read_samps += samps_per_pkt;
  }

  //Stop the stopwatch
  end_clks = radio_base_addr[RADIO_TUNER_TIMER_REG_OFFSET];

  printf("Finished! Read %d samples\r\n", read_samps);

  //Calculate elapsed time
  if (end_clks < start_clks)
    elapsed_time = ((double)(0xFFFFFFFF-start_clks+end_clks))*clk_per;
  else
    elapsed_time = ((double)(end_clks - start_clks))*clk_per;
  
  printf("Total run time = %f seconds\r\n", elapsed_time);
}

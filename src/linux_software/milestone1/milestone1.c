#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

// Constant declarations
#define PORT_NUM 25344
#define PAYLOAD_SIZE 1024

// Function prototypes
int parse_cmd_line(int argc, char *argv[], int *num_pkts, char **ip_addr);
int load_test_data(FILE *fp, char *buffer, int len);
int create_socket(int *socket_desc, struct sockaddr_in *client_addr,
                  char *ip_addr, int port_num);
int send_udp_pkt(char *payload, int len);


// Main function
int main(int argc, char *argv[]) {
  // Variables
  int socket_desc;
  struct sockaddr_in client_addr;
  char server_message[PAYLOAD_SIZE+2]; //Data payload + 2 bytes for the frame count
  int client_struct_length = sizeof(client_addr);
  int pkt_status;
  unsigned short i;

  int num_pkts = 0;
  char *ip_addr;

  char *filename = "text_src.txt";

  // Parse the command line and pull out dest. ip address and # of pkts to send
  if (parse_cmd_line(argc, argv, &num_pkts, &ip_addr) < 0) {
    printf("Error processing command line arguments. Exiting...\r\n");
    return -1;
  }

  //Open a dummy data file w/text
  FILE *fp = fopen(filename, "r");
  if (fp==NULL) {
    printf("Error: could not open file %s", filename);
    return -1;
  }
  
  // Create the socket and setup the client address
  if (create_socket(&socket_desc, &client_addr, ip_addr, PORT_NUM)) return -1;

  printf("Sending %d test UDP packets...\r\n", num_pkts);
  for (i=0; i<num_pkts; i++) {
    // Clear and load the message buffer
    memset(server_message, '\0', sizeof(server_message));
    server_message[0] = i;
    server_message[1] = i >> 8;
    load_test_data(fp, &server_message[2], PAYLOAD_SIZE);

    // Send the next packet
    if (sendto(socket_desc, server_message, sizeof(server_message), 0,
               (struct sockaddr*)&client_addr, client_struct_length) < 0) {
      printf("Error sending UDP packet %d\r\n",i);
      return -1;
    }
  }

  // Close the socket and file pointer
  close(socket_desc);
  fclose(fp);

  return 0;
}

//------------------------------------------------------------------------
// Supporting Functions
//------------------------------------------------------------------------

// Function to parse the command line for dest. ip address and num packets
int parse_cmd_line(int argc, char *argv[], int *num_pkts, char **ip_addr) {
  // Error checking
  if (!(argc==3)) {
    printf("Error: incorrect number of arguments! \r\n");
    printf("Please enter a destination IP address and the number of packets to send\r\n");
    return -1;
  }

  if (strlen(argv[1]) > 15) {
    printf("Error: Specified IP address is too long\r\n");
    return -1;
  }

  // Assign the IP addr and packet count variables
  sscanf(argv[2],"%d",num_pkts);
  *ip_addr = argv[1];

  return 0;    
}


// Function to load sample data from a text file
int load_test_data(FILE *fp, char *buffer, int len) {
  int i;
  char c;
  
  for (i=0; i<len; i++) {
    // Get the next char from the file
    c = fgetc(fp);
    
    // If we reach the end of the file, rewind back to the beginning
    if (feof(fp)) {
      fseek(fp, 0, SEEK_SET);
      c = fgetc(fp);
    }

    // Load the data into the buffer
    buffer[i] = c;
  }

  return 0;
}


// Function to create/setup the socket and client address
int create_socket(int *socket_desc, struct sockaddr_in *client_addr,
                  char *ip_addr, int port_num) {
  // Create UDP socket
  printf("Creating socket...");
  *socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (*socket_desc < 0) {
    printf("Failed! Error creating socket\r\n");
    return -1;
  }
  printf("Success!\r\n");

  // Set port and IP address
  client_addr->sin_family = AF_INET;
  client_addr->sin_addr.s_addr = inet_addr(ip_addr);
  client_addr->sin_port = htons(port_num);

  return 0;
}


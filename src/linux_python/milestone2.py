#!/usr/bin/python
import os
import myutils as util

# The goal of this script is to read 480,000 samples from the simple fifo
#  in the radio design. The read samples are just thown on the floor.
#  The idea is to demonstrate that the FIFO works as designed:
#    * Can poll the fullness reg to detect once we've hit a certain level
#    * Can empty the FIFO and it keeps filling back up from the radio
#    * Use the radio's timer to keep track of the total elapsed time
#      ** 480000/48.828kSPS ~= 10 sec

# Constants
FULL_RADIO_BASE = 0x43C00000
RADIO_TIMER = FULL_RADIO_BASE + 0xC

FIFO_BASE = 0x43C10000
FIFO_DATA_REG = FIFO_BASE + 0x0
FIFO_FILL_REG = FIFO_BASE + 0x4

SAMPS_TO_READ = 480000
SAMPS_PER_PKT = 256

# Variables
fill_lvl = 0
rd_samples = 0
rd_wrd = 0
start_clks = 0
end_clks = 0
elapsed_time = 0

# Print message to indicate test is starting
print("Hello, I am going to read 10 seconds worth of data now...")

# Start the stop watch
start_clks = util.read(RADIO_TIMER)

# Main loop
while rd_samples < SAMPS_TO_READ:
    
    # Poll the fill reg until we have 256 samples
    while fill_lvl < SAMPS_PER_PKT: fill_lvl = util.read(FIFO_FILL_REG)

    # Read a packet's worth of data out of the FIFO
    for i in range(SAMPS_PER_PKT): rd_wrd = util.read(FIFO_DATA_REG)

    # Increment the total samples read
    rd_samples = rd_samples + SAMPS_PER_PKT
    
    # **Debug** display the fill level after the reads
    fill_lvl = util.read(FIFO_FILL_REG)
    print("Fill level = " + str(fill_lvl))
    print("rd_samps = " + str(rd_samples))

# Stop the stop watch, calculate and display elapsed time
end_clks = util.read(RADIO_TIMER)
elapsed_time = (end_clks - start_clks) * (1/125e6)
print("Finished!")
print("Took " + str(elapsed_time) + " to read 480,000 samples")

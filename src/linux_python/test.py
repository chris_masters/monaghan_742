#!/usr/bin/python
import os
import time
import mmap

MAP_MASK = mmap.PAGESIZE - 1

#import myutils as util

# Constants
FULL_RADIO_BASE = 0x43C00000
RADIO_TIMER = FULL_RADIO_BASE + 0xC

FIFO_BASE = 0x43C10000
FIFO_DATA_REG = FIFO_BASE + 0x0
FIFO_FILL_REG = FIFO_BASE + 0x4

addr = FIFO_FILL_REG
f = os.open("/dev/mem", os.O_RDWR | os.O_SYNC)
mem = mmap.mmap(f, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE,offset=addr & ~MAP_MASK)

fill_lvl = util.read(FIFO_FILL_REG)
print("Fill level = " + str(fill_lvl))

for i in range(1024):
    util.read(FIFO_DATA_REG)

for i in range(10):
    #time.sleep(1)
    #fill_lvl = util.read(FIFO_FILL_REG)
    print(util.read(FIFO_FILL_REG))
    #fill_lvl = 0

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/20/2022 07:55:55 PM
-- Design Name: 
-- Module Name: toplevel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--use work.soc_sdr_pkg.all;

entity toplevel is
  port (
    -- Do NOT need constraints
    DDR_addr          : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba            : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n         : inout STD_LOGIC;
    DDR_ck_n          : inout STD_LOGIC;
    DDR_ck_p          : inout STD_LOGIC;
    DDR_cke           : inout STD_LOGIC;
    DDR_cs_n          : inout STD_LOGIC;
    DDR_dm            : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq            : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n         : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p         : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt           : inout STD_LOGIC;
    DDR_ras_n         : inout STD_LOGIC;
    DDR_reset_n       : inout STD_LOGIC;
    DDR_we_n          : inout STD_LOGIC;
    FIXED_IO_ddr_vrn  : inout STD_LOGIC;
    FIXED_IO_ddr_vrp  : inout STD_LOGIC;
    FIXED_IO_mio      : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk   : inout STD_LOGIC;
    FIXED_IO_ps_porb  : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;

    -- Need constraints
    ac_bclk           : out STD_LOGIC;
    ac_mclk           : out STD_LOGIC;
    ac_muten          : out STD_LOGIC;
    ac_pbdat          : out STD_LOGIC;
    ac_pblrc          : out STD_LOGIC;
    ac_scl            : inout STD_LOGIC;
    ac_sda            : inout STD_LOGIC;
    reset_pb          : in STD_LOGIC;
    sysclk            : in STD_LOGIC
    );
end toplevel;

architecture structural of toplevel is
  component proc_system is
    port (
      sdata               : out STD_LOGIC;
      lrck                : out STD_LOGIC;
      bclk                : out STD_LOGIC;
      mclk                : out STD_LOGIC;
      iic_rtl_scl_i       : in STD_LOGIC;
      iic_rtl_scl_o       : out STD_LOGIC;
      iic_rtl_scl_t       : out STD_LOGIC;
      iic_rtl_sda_i       : in STD_LOGIC;
      iic_rtl_sda_o       : out STD_LOGIC;
      iic_rtl_sda_t       : out STD_LOGIC;
      DDR_cas_n           : inout STD_LOGIC;
      DDR_cke             : inout STD_LOGIC;
      DDR_ck_n            : inout STD_LOGIC;
      DDR_ck_p            : inout STD_LOGIC;
      DDR_cs_n            : inout STD_LOGIC;
      DDR_reset_n         : inout STD_LOGIC;
      DDR_odt             : inout STD_LOGIC;
      DDR_ras_n           : inout STD_LOGIC;
      DDR_we_n            : inout STD_LOGIC;
      DDR_ba              : inout STD_LOGIC_VECTOR ( 2 downto 0 );
      DDR_addr            : inout STD_LOGIC_VECTOR ( 14 downto 0 );
      DDR_dm              : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dq              : inout STD_LOGIC_VECTOR ( 31 downto 0 );
      DDR_dqs_n           : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dqs_p           : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      FIXED_IO_mio        : inout STD_LOGIC_VECTOR ( 53 downto 0 );
      FIXED_IO_ddr_vrn    : inout STD_LOGIC;
      FIXED_IO_ddr_vrp    : inout STD_LOGIC;
      FIXED_IO_ps_srstb   : inout STD_LOGIC;
      FIXED_IO_ps_clk     : inout STD_LOGIC;
      FIXED_IO_ps_porb    : inout STD_LOGIC
      );
  end component proc_system;
  
  component IOBUF is
    port (
      I   : in STD_LOGIC;
      O   : out STD_LOGIC;
      T   : in STD_LOGIC;
      IO  : inout STD_LOGIC
      );
  end component IOBUF;

  --signal ctr              : unsigned(3 downto 0);
  --signal clk,rst,ena_2Hz  : std_logic;
  --signal FCLK_CLK0        : std_logic;

  -- IIC signals
  signal iic_rtl_scl_i    : std_logic;
  signal iic_rtl_scl_o    : std_logic;
  signal iic_rtl_scl_t    : std_logic;
  signal iic_rtl_sda_i    : std_logic;
  signal iic_rtl_sda_o    : std_logic;
  signal iic_rtl_sda_t    : std_logic;

  -- DAC output signals
  --signal m_axis_data_tdata   : std_logic_vector(31 downto 0);
  --signal m_axis_data_tvalid  : std_logic;


begin
----------------------------------------------------------------------------------
-- Processing system
----------------------------------------------------------------------------------
  proc_system_i: component proc_system
    port map (
      sdata                         => ac_pbdat,
      lrck                          => ac_pblrc,
      bclk                          => ac_bclk,
      mclk                          => ac_mclk,
      iic_rtl_scl_i                 => iic_rtl_scl_i,
      iic_rtl_scl_o                 => iic_rtl_scl_o,
      iic_rtl_scl_t                 => iic_rtl_scl_t,
      iic_rtl_sda_i                 => iic_rtl_sda_i,
      iic_rtl_sda_o                 => iic_rtl_sda_o,
      iic_rtl_sda_t                 => iic_rtl_sda_t,
      DDR_addr(14 downto 0)         => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0)            => DDR_ba(2 downto 0),
      DDR_cas_n                     => DDR_cas_n,
      DDR_ck_n                      => DDR_ck_n,
      DDR_ck_p                      => DDR_ck_p,
      DDR_cke                       => DDR_cke,
      DDR_cs_n                      => DDR_cs_n,
      DDR_dm(3 downto 0)            => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0)           => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0)         => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0)         => DDR_dqs_p(3 downto 0),
      DDR_odt                       => DDR_odt,
      DDR_ras_n                     => DDR_ras_n,
      DDR_reset_n                   => DDR_reset_n,
      DDR_we_n                      => DDR_we_n,
      FIXED_IO_ddr_vrn              => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp              => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0)     => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk               => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb              => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb             => FIXED_IO_ps_srstb
      );

  ac_muten <= '1';
  --clk <= FCLK_CLK0;
  --rst <= reset_pb;

----------------------------------------------------------------------------------
-- IIC tri-state buffers
----------------------------------------------------------------------------------
  iic_rtl_scl_iobuf: component IOBUF
    port map (
      I => iic_rtl_scl_o,
      IO => ac_scl,
      O => iic_rtl_scl_i,
      T => iic_rtl_scl_t
      );
  iic_rtl_sda_iobuf: component IOBUF
    port map (
      I => iic_rtl_sda_o,
      IO => ac_sda,
      O => iic_rtl_sda_i,
      T => iic_rtl_sda_t
      );

  
----------------------------------------------------------------------------------
-- Low-level DAC interface
----------------------------------------------------------------------------------
  -- The m_axis_data_tdata signal is constucted of the 16-bit Q output in the
  --  upper 16-bits, [31:16] and the I output in the lower 16-bits, [15:0]
  --  which puts: Q=>Left, I=>Right
  
  --dac_intrf : component lowlevel_dac_intfc_0
  --  port map (
  --    rst           => rst, --**FIX: Best is DDS reset but not here anymore
  --    clk125        => clk,
  --    data_word     => m_axis_data_tdata,
  --    sdata         => ac_pbdat,
  --    lrck          => ac_pblrc,
  --    bclk          => ac_bclk,
  --    mclk          => ac_mclk,
  --    latched_data  => open,
  --    valid         => m_axis_data_tvalid
  --    );



----------------------------------------------------------------------------------
-- ILA
----------------------------------------------------------------------------------
  --lab6_ila : ila_0
  --  port map (
  --    clk       => clk,
  --    probe0(0) => m_axis_data_tvalid,
  --    probe1    => m_axis_data_tdata(31 downto 16),
  --    probe2    => m_axis_data_tdata(15 downto 0)
  --    );
  
  --lab2_ila : ila_0
  --  PORT MAP (
  --    clk         => clk,
  --
  --    --Fake ADC output
  --    probe0      => adc_data_o,      --16-bits
  --
  --    --Tuner DDS output
  --    probe1      => tune_dds_data_o, --32-bits
  --
  --    --Mixer output
  --    probe2      => mix_out_Q(28 downto 13), --16-bits
  --    probe3      => mix_out_I(28 downto 13), --16-bits
  --    
  --    --Outputs from filter 0
  --    probe4      => filt0_data_o(92 downto 77),  --16-bits
  --    probe5      => filt0_data_o(44 downto 29),  --16-bits
  --    probe6(0)   => filt0_valid_o,
  --
  --    --Outputs from filter 1
  --    probe7      => filt1_data_o(93 downto 78),  --16-bits
  --    probe8      => filt1_data_o(45 downto 30),  --16-bits
  --    probe9(0)   => filt1_valid_o,
  --
  --    --DAC input
  --    probe10     => dac_data   --32-bits
  --    );

end structural;


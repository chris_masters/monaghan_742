----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/12/2022 07:55:55 PM
-- Design Name: 
-- Module Name: soc_sdr_pkg.vhd
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Package file for the SoC course SDR designs
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.vcomponents.all;

package soc_sdr_pkg is

  -----------------------------------
  -- Component Declarations
  -----------------------------------
  component proc_system is
    port (
      FCLK_CLK0             : out std_logic;
      iic_rtl_scl_i         : in std_logic;
      iic_rtl_scl_o         : out std_logic;
      iic_rtl_scl_t         : out std_logic;
      iic_rtl_sda_i         : in std_logic;
      iic_rtl_sda_o         : out std_logic;
      iic_rtl_sda_t         : out std_logic;
      DDR_cas_n             : inout STD_LOGIC;
      DDR_cke               : inout STD_LOGIC;
      DDR_ck_n              : inout STD_LOGIC;
      DDR_ck_p              : inout STD_LOGIC;
      DDR_cs_n              : inout STD_LOGIC;
      DDR_reset_n           : inout STD_LOGIC;
      DDR_odt               : inout STD_LOGIC;
      DDR_ras_n             : inout STD_LOGIC;
      DDR_we_n              : inout STD_LOGIC;
      DDR_ba                : inout STD_LOGIC_VECTOR ( 2 downto 0 );
      DDR_addr              : inout STD_LOGIC_VECTOR ( 14 downto 0 );
      DDR_dm                : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dq                : inout STD_LOGIC_VECTOR ( 31 downto 0 );
      DDR_dqs_n             : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dqs_p             : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      FIXED_IO_mio          : inout STD_LOGIC_VECTOR ( 53 downto 0 );
      FIXED_IO_ddr_vrn      : inout STD_LOGIC;
      FIXED_IO_ddr_vrp      : inout STD_LOGIC;
      FIXED_IO_ps_srstb     : inout STD_LOGIC;
      FIXED_IO_ps_clk       : inout STD_LOGIC;
      FIXED_IO_ps_porb      : inout STD_LOGIC;
      GPIO_0_tri_o          : out STD_LOGIC_VECTOR(31 downto 0);
      GPIO2_0_tri_o         : out STD_LOGIC_VECTOR(31 downto 0)
      );
  end component proc_system;

  component IOBUF is
    port (
      I : in STD_LOGIC;
      O : out STD_LOGIC;
      T : in STD_LOGIC;
      IO : inout STD_LOGIC
      );
  end component IOBUF;

  COMPONENT lowlevel_dac_intfc_0
    PORT (
      rst           : IN STD_LOGIC;
      clk125        : IN STD_LOGIC;
      data_word     : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      sdata         : OUT STD_LOGIC;
      lrck          : OUT STD_LOGIC;
      bclk          : OUT STD_LOGIC;
      mclk          : OUT STD_LOGIC;
      latched_data  : OUT STD_LOGIC;
      valid         : IN STD_LOGIC 
      );
  END COMPONENT;

  COMPONENT ila_0
    PORT (
      clk : IN STD_LOGIC;
      probe0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
      probe2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
      probe7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe8 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
      probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      probe10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
  END COMPONENT  ;

  COMPONENT dds_compiler_0
    PORT (
      aclk                : IN STD_LOGIC;
      aresetn             : IN STD_LOGIC;
      s_axis_phase_tvalid : IN STD_LOGIC;
      s_axis_phase_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axis_data_tvalid  : OUT STD_LOGIC;
      m_axis_data_tdata   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0) 
      );
  END COMPONENT;

  COMPONENT dds_compiler_1
    PORT (
      aclk                : IN STD_LOGIC;
      aresetn             : IN STD_LOGIC;
      s_axis_phase_tvalid : IN STD_LOGIC;
      s_axis_phase_tdata  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axis_data_tvalid  : OUT STD_LOGIC;
      m_axis_data_tdata   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0) 
      );
  END COMPONENT;

  COMPONENT fir_compiler_0
    PORT (
      aclk                : IN STD_LOGIC;
      s_axis_data_tvalid  : IN STD_LOGIC;
      s_axis_data_tready  : OUT STD_LOGIC;
      s_axis_data_tdata   : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axis_data_tvalid  : OUT STD_LOGIC;
      m_axis_data_tdata   : OUT STD_LOGIC_VECTOR(95 DOWNTO 0) 
      );
  END COMPONENT;
  
  COMPONENT fir_compiler_1
    PORT (
      aclk                : IN STD_LOGIC;
      s_axis_data_tvalid  : IN STD_LOGIC;
      s_axis_data_tready  : OUT STD_LOGIC;
      s_axis_data_tdata   : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axis_data_tvalid  : OUT STD_LOGIC;
      m_axis_data_tdata   : OUT STD_LOGIC_VECTOR(95 DOWNTO 0) 
      );
  END COMPONENT;
  

end package;

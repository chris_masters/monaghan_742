----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/27/2022 08:35:06 PM
-- Design Name: 
-- Module Name: full_radio_top - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.soc_sdr_pkg.all;

entity full_radio_top is
  port (
    clk                 : in std_logic;
    rstn                : in std_logic;
    -- Control inputs
    fake_adc_pinc_reg   : in std_logic_vector(31 downto 0);
    tuner_pinc_reg      : in std_logic_vector(31 downto 0);
    ctrl_reg            : in std_logic_vector(31 downto 0);
    -- Timer output
    timer_out           : out std_logic_vector(31 downto 0);
    -- AXI stream output
    m_axis_data_tvalid  : out std_logic;
    m_axis_data_tdata   : out std_logic_vector(31 downto 0)
    );
end full_radio_top;

architecture rtl of full_radio_top is
  -- Fake ADC DDS signals
  signal dds_rst_n        : std_logic;
  signal dds_valid_o      : std_logic;
  signal adc_data_o       : std_logic_vector(15 downto 0);
  signal adc_data_sg      : signed(15 downto 0);

  -- Tuner DDS signals
  signal tune_dds_val_o   : std_logic;
  signal tune_dds_data_o  : std_logic_vector(31 downto 0);
  signal tune_dds_I       : signed(15 downto 0);
  signal tune_dds_Q       : signed(15 downto 0);

  -- Mixer signals
  signal mix_out_I        : std_logic_vector(31 downto 0);
  signal mix_out_Q        : std_logic_vector(31 downto 0);

  -- Filter signals
  signal filt0_valid_o    : std_logic;
  signal filt0_data_o     : std_logic_vector(95 downto 0);
  signal filt1_data_i     : std_logic_vector(31 downto 0);
  signal filt1_valid_o    : std_logic;
  signal filt1_data_o     : std_logic_vector(95 downto 0);

  -- Timer signals
  signal tmr_count        : unsigned(31 downto 0);
  
begin
----------------------------------------------------------------------------------
-- "Fake ADC" DDS
----------------------------------------------------------------------------------
  dds_rst_n <= not ctrl_reg(0);
  
  adc_inst : dds_compiler_0
    PORT MAP (
      aclk                => clk,
      aresetn             => dds_rst_n,
      s_axis_phase_tvalid => '1',
      s_axis_phase_tdata  => fake_adc_pinc_reg,
      m_axis_data_tvalid  => dds_valid_o,
      m_axis_data_tdata   => adc_data_o
      );
  
  
----------------------------------------------------------------------------------
-- Tuner DDS
----------------------------------------------------------------------------------
  dds_inst : dds_compiler_1
    PORT MAP (
      aclk                => clk,
      aresetn             => dds_rst_n,
      s_axis_phase_tvalid => '1',
      s_axis_phase_tdata  => tuner_pinc_reg,
      m_axis_data_tvalid  => tune_dds_val_o,
      m_axis_data_tdata   => tune_dds_data_o  --I on [15:0], Q on [31:16]
      );
  
  
----------------------------------------------------------------------------------
-- Mixer: Multiply the fake ADC output (real sig) by the tuneable
--  DDS output (complex)
----------------------------------------------------------------------------------
  tune_dds_I  <= signed(tune_dds_data_o(15 downto 0));
  tune_dds_Q  <= signed(tune_dds_data_o(31 downto 16));
  adc_data_sg <= signed(adc_data_o);
    
  mix_out_I <=  std_logic_vector(tune_dds_I * adc_data_sg);
  mix_out_Q <=  std_logic_vector(tune_dds_Q * adc_data_sg);
  

----------------------------------------------------------------------------------
-- First stage filter: dual-path, decimate by 40
--  Note: Sticking w/the DDS output convention - Q is bits 31:16, I is 15:0
--  Also, performing a divide-by-16384 operation by right-shifting by 14-bits
--  i.e., select bits 29:14. This is to maintain unity gain
----------------------------------------------------------------------------------
  filt0 : fir_compiler_0
    port map (
      aclk                => clk,
      s_axis_data_tvalid  => dds_valid_o,
      s_axis_data_tready  => open,
      s_axis_data_tdata   => mix_out_Q(29 downto 14) & mix_out_I(29 downto 14),
      m_axis_data_tvalid  => filt0_valid_o,
      m_axis_data_tdata   => filt0_data_o
      );
  

----------------------------------------------------------------------------------
-- Second stage filter: dual-path, decimate by 64
----------------------------------------------------------------------------------
  --These are the bits we need from filter 0 for unity gain
  --i.e., output frac. bits = 29
  filt1_data_i(31 downto 16) <= filt0_data_o(92 downto 77); --Q
  filt1_data_i(15 downto 0)  <= filt0_data_o(44 downto 29); --I
  
  filt1 : fir_compiler_1
    port map (
      aclk                => clk,
      s_axis_data_tvalid  => filt0_valid_o,
      s_axis_data_tready  => open,
      s_axis_data_tdata   => filt1_data_i,
      m_axis_data_tvalid  => filt1_valid_o,
      m_axis_data_tdata   => filt1_data_o
      );
  
----------------------------------------------------------------------------------
-- Output assignments
----------------------------------------------------------------------------------
  m_axis_data_tdata <= filt1_data_o(93 downto 78) &
                       filt1_data_o(45 downto 30);
  m_axis_data_tvalid <= filt1_valid_o;
  

----------------------------------------------------------------------------------
-- Timer: 32-bit free running counter, counts 125MHz clocks
----------------------------------------------------------------------------------
  tmr_proc : process (clk, rstn) is
  begin
    if (rstn = '0') then
      tmr_count <= (others => '0');
    elsif (rising_edge(clk)) then
      tmr_count <= tmr_count + 1;
    end if;
  end process tmr_proc;

  timer_out <= std_logic_vector(tmr_count);
  
end rtl;
